#!/usr/bin/python3

from flask import Flask, jsonify
from flask import request
from driver import Driver
from apiparser2v0 import ApiParser2v0
from RegionDataBase import  RegionsDatabase
from TaskController import TaskController
from flask_cors import CORS
from flask import abort

import json

app = Flask(__name__)
CORS(app)

'''
  API 2.0
'''
@app.route('/apiv2.0/regions', methods=['GET'])
def parse_get_all_regions():
    status, regions = api_parser.parse_get_all_regions()
    return jsonify({"status": status, "regions": regions})


@app.route('/apiv2.0/region', methods=['POST'])
def add_region():
    status, region_id = api_parser.parse_add_region(json.loads(request.data))
    controller.process_regions()
    return jsonify({"status": status, "id": region_id})


@app.route('/apiv2.0/region/<int:id>', methods=['DELETE'])
def remove_region(id):
    status = api_parser.parse_remove_region(id)
    controller.process_regions()
    if status != "ok":
        abort(400, 'Id not found')
    return jsonify({"status": status})


@app.route('/apiv2.0/update_bulk_regions', methods=['POST'])
def update_bulk_regions():
    pass


@app.route('/apiv2.0/region/<int:id>', methods=['PUT'])
def update_region(id):
    status = api_parser.parse_update_region(id, json.loads(request.data))
    controller.process_regions()
    return jsonify({"status": status})


@app.route('/apiv2.0/get_region', methods=['POST'])
def get_region():
    pass


@app.route('/apiv2.0/get_all_regions', methods=['POST'])
def get_all_regions():
    pass



if __name__ == "__main__":

    driver = Driver()
    database = RegionsDatabase()
    controller = TaskController(driver, database)
    # actions should be put in some list and executed by some controller ...
    api_parser = ApiParser2v0(database)

    app.run(host='0.0.0.0')
