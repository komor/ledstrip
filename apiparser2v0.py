
from data_structures import Range, Color
from RegionDataBase import RegionsDatabase
from constants import *


class ApiParser2v0:

    def __init__(self, database: RegionsDatabase):
        self.database = database

    def parse_add_region(self, json):
        status = "error"
        region_id = None
        status, region_id = self.database.add_region(json['region'])

        return status, region_id

    def parse_remove_region(self, _id):

        status = "error"
        status = self.database.remove_region(_id)
        return status

    def parse_update_bulk_regions(self, json):
        pass

    def parse_update_region(self, _id, json):
        status = self.database.update_region(_id, json)
        return status

    def parse_get_region(self, json):
        pass

    def parse_get_all_regions(self):

        status, regions = self.database.get_all_regions()
        return status, regions


