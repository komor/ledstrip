import math
from constants import *

DBG = False


class MockPixels:

    def __init__(self, num):
        self.pixels = [(0, 0, 0)] * num

    def __setitem__(self, index, val):
        self.pixels[index] = val

    def show(self):
        return
        i = 0
        for p in self.pixels:
            print(i, end=':')
            print(p)
            i += 1

    def fill(self, val):

        for i in range(0, len(self.pixels)):

            self.pixels[i] = val


class Driver:

    def __init__(self):

        if DBG:
            self.pixels = MockPixels(pixel_number)

        else:
            import board
            import neopixel

            pixel_pin = board.D18
            num_pixels = pixel_number
            ORDER = neopixel.GRB

            self.pixels = neopixel.NeoPixel(pixel_pin, num_pixels, brightness=1.0, auto_write=False,
                                       pixel_order=ORDER)

    def process_regions(self, regions):

        self.pixels.fill((0, 0, 0))
        # self.pixels.show()

        for r in regions:
            if r[state_str] == valid_states[on_index]:
                b = r[brightness_str] / 100
                for i in range(r[range_str][low_index_str], r[range_str][high_index_str]):
                    if r['fx'] ['type'] == 'color':
                        c = r['fx']['params']['color']
                    self.pixels[i] = (int(b * c['r']), int(b * c['g']), int(b * c['b']))

                self.pixels.show()
            else:
                for i in range(r[range_str][low_index_str], r[range_str][high_index_str]):
                    self.pixels[i] = (0, 0, 0)
                self.pixels.show()

        return "ok"



