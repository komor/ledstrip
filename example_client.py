from requests import Request, Session
from string import Template
import time

url = 'http://192.168.1.48:5000/apiv1.0'
data = Template("""{
    "mode" : "regions",
    "regions": [
    {
    	"state" : "on",
        "brightness" : ${bri},
        "color" : { "r" : 255, "g" : 255, "b" : 255},
        "range" : { "li" : ${lind} , "hi" : ${h}}
    },
    {
    	"state" : "off",
        "range" : { "li" : 0 , "hi" : ${end}}
    }
    
    ]
}""")

s = Session()
s.headers = {"Content-Type": "application/json"}

d = data.substitute(bri=1, lind=10, h=150, end=10)
s.post(url, d)

# ts = time.time()
# for i in range(0, 100):
#     d = data.substitute(bri=2, lind=0, h=i, end=1)
#    # s.post(url, d)
#     #d = data.substitute(bri=0, lind=0, h=100, end=1)
#     s.post(url, d)
#
# te = time.time()
# print(te - ts)
s.close()