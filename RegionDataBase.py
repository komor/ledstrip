import json

class RegionsDatabase:

    def __init__(self):
        self.regions = []
        self.id = -1

    def calculate_id(self):
        self.id += 1
        return self.id

    def add_region(self, data):
        region_id = self.calculate_id()
        # check data and add to it id
        data['id'] = region_id
        self.regions.append(data)
        status = "ok"
        return status, region_id

    def remove_region(self, region_id):
        if len(self.regions):
            status = "ok"
        else:
            status = "no regions"
        try:
            for r in self.regions:
                if r['id'] == region_id:
                    self.regions.remove(r)
                    break
                status = "no id"
        except:
            status = "some error"

        return status

    def update_region(self, _id, json):
        status = None
        # find element which have id, in database like mysql thois should be simpler ...
        status = "ok"
        print(json)
        try:
            for r in self.regions:
                if r['id'] == _id:
                    self.regions.remove(r)
                    self.regions.append(json['region'])
                    break
                status = "no id"
        except:
            status = "some error"

        return status

    def get_region(self, region_id):
        pass

    def get_all_regions(self):
        regions_str = json.dumps([ob for ob in self.regions])
        return "ok", self.regions
