
ranges_index = 0
valid_modes = ["regions"]
pixel_number = 150

on_index = 0
off_index = 1
valid_states = ["on", "off"]

regions_str = 'regions'
color_str = 'color'
range_str = 'range'
low_index_str = 'li'
high_index_str = 'hi'
mode_str = 'mode'
state_str = 'state'
brightness_str = 'brightness'
red_str = 'r'
green_str ='g'
blue_str = 'b'