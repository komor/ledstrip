# example json for api 1v0
# {
#     "mode" : "regions",
#     "regions": [
#         {
#             "state": "on",
#
#             "brightness": 1.0,
#             "color": {"r": 255, "g": 255, "b": 255},
#             "range": {"li": 0, "hi": 20}
#         },
#
#         {
#             "state": "off",
#
#             "brightness": 0.3,
#             "color": {"r": 255, "g": 255, "b": 255},
#             "range": {"li": 30, "hi": 50}
#         }
#
#     ]
# }

# for each region can be added fx like, dimmer in time, rainbow, graDIENT, PINGPONG

from data_structures import Range, Color
from constants import *


class ApiParser1v0:

    def __init__(self, led_driver):
        self.led_driver = led_driver

    def check_data_validity(self, json):

        valid_brightness = range(0, 101, 1)
        valid_color_value = range(0, 256, 1)
        valid_index = range(0, pixel_number + 1, 1)

        if json[mode_str] not in valid_modes:
            return "unknown mode"

        for r in json[regions_str]:
            if r[state_str] not in valid_states:
                return "some region don't contains valid state"
            elif r[range_str][low_index_str] not in valid_index:
                return "range don't contains valid low index"
            elif r[range_str][high_index_str] not in valid_index:
                return "range don't contains valid high index"
            elif r[range_str][low_index_str] >= r[range_str][high_index_str]:
                return " low index must be lower than high index"
            elif r[state_str] == valid_states[on_index]:
                if r[brightness_str] not in valid_brightness:
                    return "some region don't contains valid brightness"
                elif r[color_str][red_str] not in valid_color_value:
                    return "no valid r in color"
                elif r[color_str][green_str] not in valid_color_value:
                    return "no valid g in color"
                elif r[color_str][blue_str] not in valid_color_value:
                    return "no valid b in color"

        return "ok"

    def apiparser1v0.pyparse_request(self, json):
        regions = []

        if mode_str not in json:
            return "json don't contains mode key"
        else:
            if regions_str not in json:
                return "json don't contains any region"
            else:
                for r in json[regions_str]:
                    if state_str not in r:
                        return "some region don't contains state"
                    elif range_str not in r:
                        return "region don't contains range"
                    elif low_index_str not in r[range_str]:
                        return "range don't contains low index"
                    elif high_index_str not in r[range_str]:
                        return "range don't contains high index"
                    elif r[state_str] == valid_states[on_index]:
                        if brightness_str not in r:
                            return "some region don't contains brightness"
                        elif color_str not in r:
                            return "some region don't contains color"
                        elif red_str not in r[color_str]:
                            return "no r in color"
                        elif green_str not in r[color_str]:
                            return "no g in color"
                        elif blue_str not in r[color_str]:
                            return "no b in color"

        ret = self.check_data_validity(json)

        if ret == "ok":

            if json[mode_str] == valid_modes[ranges_index]:
                # execute ranges mode
                for r in json[regions_str]:
                    if r[state_str] == valid_states[on_index]:
                        c = r[color_str]
                        rg = r[range_str]
                        b = r[brightness_str]
                        s = r[state_str]
                        color = Color(c[red_str], c[green_str], c[blue_str])
                        regions.append(Range(s, rg[low_index_str], rg[high_index_str], b, color))
                    else:
                        rg = r[range_str]
                        s = r[state_str]
                        regions.append(Range(s, rg[low_index_str], rg[high_index_str]))

                if self.led_driver is not None:
                    return self.led_driver.process_regions(regions)

                return "debug mode"
        else:
            return ret


if __name__ == "__main__":
    p = ApiParser1v0(None)
    ret = p.parse_request({
        "mode": "regions",
        "regions": [
            {
                "state": "on",
                "brightness": 1,
                "color": {"r": 255, "g": 255, "b": 255},
                "range": {"li": 0, "hi": 20}
            },

            {
                "state": "off",
                "range": {"li": 30, "hi": 50}
            }

        ]
    })

    print(ret)
