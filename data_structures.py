

class Color:

    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b


class Range:

    def __init__(self, state, start_index, end_index,  brightness=0, color=None):
        self.state = state
        self.brightness = brightness
        self.start_index = start_index
        self.end_index = end_index
        self.color = color

# introduced in api 2.0
# {
#
#     "regions": [
#         {
#             "id": 0,
#             "state": "on"
#             "brightness": 1.0,
#             "range": {"li": 0, "hi": 20},
#             "fx": {
#               "type": "color",
#               "params": {
#                  "color": {"r": 255, "g": 255, "b": 255}
#               }
#             }
#         }
#
#      ]
# }


class Effect:

    def __init__(self, type, params):
        self.type = type
        self.params = params


class RegionRange:

    def __init__(self, start_index, end_index):
        self.start_index = start_index
        self.end_index = end_index


class Region:

    def __init__(self, _id: int, state: str, _range: RegionRange, level: int, fx: Effect):
        self.id = _id
        self.state = state
        self.range = _range
        self.level = level
        self.fx = fx


