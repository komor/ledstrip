
class TaskController:
    def __init__(self, driver, database):
        self.driver = driver
        self.database = database

    def process_regions(self):
        self.driver.process_regions(self.database.regions)